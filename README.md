## Hi I'm orozot 👏 

Welcome to my profile! I'm a front-end engineer working for JiHu(GitLab).

## Interests

<img src="https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E" height="32px">

## Dev Environment

![Macbook](https://img.shields.io/badge/-Macbook-black?style=for-the-badge&logo=apple) 


## GitLab Statistics
![GitLab Stats](https://gitlab-readme-stats.vercel.app/api?username=orozot&show_icons=true&theme=dracula)
